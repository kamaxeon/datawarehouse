"""Test cron.py."""
import datetime
from unittest import mock

from django.contrib.auth import get_user_model
from django.test.utils import override_settings
from django.utils import timezone

from datawarehouse import cron
from datawarehouse import models
from tests import utils


class TestDeleteArtifacts(utils.TestCase):
    """Test cron.delete_expired_artifacts method."""

    @staticmethod
    def _expiry_in(valid_for):
        """Calculate expiry date."""
        return timezone.now() + datetime.timedelta(days=valid_for)

    def test_delete(self):
        """Test delete_expired_artifacts deletes expired artifacts."""
        models.Artifact.objects.bulk_create([
            # Expired
            models.Artifact(name='test_1', url='http://test_1', valid_for=1, expiry_date=self._expiry_in(-2)),
            models.Artifact(name='test_2', url='http://test_2', valid_for=1, expiry_date=self._expiry_in(-1)),
            # Still valid
            models.Artifact(name='test_3', url='http://test_3', valid_for=1, expiry_date=self._expiry_in(1)),
            models.Artifact(name='test_4', url='http://test_4', valid_for=1, expiry_date=self._expiry_in(2)),
        ])

        self.assertEqual(4, models.Artifact.objects.count())
        cron.delete_expired_artifacts()
        self.assertEqual(2, models.Artifact.objects.count())

        # These would raise if not present.
        models.Artifact.objects.get(name='test_3')
        models.Artifact.objects.get(name='test_4')


class TestDeleteOldNotConfirmedUsers(utils.TestCase):
    """Test cron.delete_old_not_confirmed_users method"""

    @staticmethod
    def _shifted_days(days):
        """Calculate shifted date."""
        return timezone.now() + datetime.timedelta(days=days)

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=30)
    def test_delete(self):
        """Test if old invalid users get deleted"""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="old_test_1", date_joined=self._shifted_days(-32)),
            User(username="old_test_2", date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="old_test_3", date_joined=self._shifted_days(-15)),
            User(username="old_test_4", date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.delete_old_not_confirmed_users()
        self.assertEqual(3, User.objects.count())

        User.objects.get(username="old_test_2")
        User.objects.get(username="old_test_3")
        User.objects.get(username="old_test_4")

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=None)
    def test_delete_disabled(self):
        """Test if old invalid users get deleted"""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="old_test_1", date_joined=self._shifted_days(-32)),
            User(username="old_test_2", date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="old_test_3", date_joined=self._shifted_days(-15)),
            User(username="old_test_4", date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.delete_old_not_confirmed_users()
        self.assertEqual(4, User.objects.count())

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=30)
    @mock.patch('datawarehouse.utils.async_send_email.delay')
    def test_send_deletion_notification(self, send_mock):
        """Test if email gets sent."""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="del_test_1", email="del1@mail.com",
                 date_joined=self._shifted_days(-32)),
            User(username="del_test_2", email="del2@mail.com",
                 date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="del_test_3", email="del3@mail.com",
                 date_joined=self._shifted_days(-15)),
            User(username="del_test_4", email="del4@mail.com",
                 date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.send_account_deletion_warning()

        # Check if mails have been sent to correct recipients
        self.assertEqual(2, send_mock.call_count)
        calls = [
            mock.call(mock.ANY, mock.ANY, "del1@mail.com"),
            mock.call(mock.ANY, mock.ANY, "del3@mail.com")
        ]
        send_mock.assert_has_calls(calls, any_order=True)

        # Check if we don't send multiple emails
        cron.send_account_deletion_warning()
        self.assertEqual(2, send_mock.call_count)

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=0)
    @mock.patch('datawarehouse.utils.async_send_email.delay')
    def test_send_deletion_notification_disabled(self, send_mock):
        """Test if email doesn't get sent."""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="del_test_1", email="del1@mail.com",
                 date_joined=self._shifted_days(-32)),
            User(username="del_test_2", email="del2@mail.com",
                 date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="del_test_3", email="del3@mail.com",
                 date_joined=self._shifted_days(-15)),
            User(username="del_test_4", email="del4@mail.com",
                 date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.send_account_deletion_warning()
        self.assertEqual(0, send_mock.call_count)
