"""Test the signal_receivers module."""
from unittest import mock

from django import test
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse import signal_receivers
from datawarehouse import signals
from datawarehouse.api.kcidb import serializers as kcidb_serializers


class SignalsTest(test.TestCase):
    """Unit tests for the Signals module."""

    @staticmethod
    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_message(send_kcidb):
        """Test that the kcidb message is sent correctly."""
        rev = models.KCIDBCheckout.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        signal_receivers.send_kcidb_object_message(
            status='some-status',
            object_type='checkout',
            objects=[rev],
        )
        send_kcidb.assert_called_with([{
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'some-status',
            'object_type': 'checkout',
            'object': kcidb_serializers.KCIDBCheckoutSerializer(rev).data,
            'id': rev.id,
            'iid': rev.iid,
        }])

    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification', mock.Mock())
    def test_signal_kcidb_object(self):
        """Test kcidb_object signal receivers."""
        rev = models.KCIDBCheckout.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        receivers = signals.kcidb_object.send(
            sender='test', status='test', object_type='checkout', objects=[rev]
        )
        self.assertEqual(
            [(signal_receivers.send_kcidb_object_message, None)],
            receivers
        )


class IssueOccurrenceAssigned(test.TestCase):
    """
    Test issue_occurrence_assigned receiver.

    Ensure necessary functions are called.
    """

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up test."""
        self.issue = models.Issue.objects.last()

    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_checkout(self, update_related_checkout, update_issue_policy):
        """"Test issue_occurrence_assigned receiver with checkout."""
        checkout = models.KCIDBCheckout.objects.last()

        checkout.issues.add(self.issue)
        update_related_checkout.assert_called_with(checkout, {self.issue.id})
        update_issue_policy.assert_called_with({self.issue.id})

    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_build(self, update_related_checkout, update_issue_policy):
        """"Test issue_occurrence_assigned receiver with build."""
        build = models.KCIDBBuild.objects.last()

        build.issues.add(self.issue)
        update_related_checkout.assert_called_with(build, {self.issue.id})
        update_issue_policy.assert_called_with({self.issue.id})

    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_test(self, update_related_checkout, update_issue_policy):
        """"Test issue_occurrence_assigned receiver with test."""
        test = models.KCIDBTest.objects.last()

        test.issues.add(self.issue)
        update_related_checkout.assert_called_with(test, {self.issue.id})
        update_issue_policy.assert_called_with({self.issue.id})

    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_multiple_add(self, update_related_checkout, update_issue_policy):
        """"Test issue_occurrence_assigned receiver when adding multiple objects."""
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        test.issues.add(*issues)
        update_related_checkout.assert_called_with(test, {i.id for i in issues})
        update_issue_policy.assert_called_with({i.id for i in issues})

    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_policy')
    @mock.patch('datawarehouse.signal_receivers.scripts.update_issue_occurrences_related_checkout')
    def test_multiple_set(self, update_related_checkout, update_issue_policy):
        """"Test issue_occurrence_assigned receiver when setting multiple objects."""
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        test.issues.set(issues)
        update_related_checkout.assert_called_with(test, {i.id for i in issues})
        update_issue_policy.assert_called_with({i.id for i in issues})
