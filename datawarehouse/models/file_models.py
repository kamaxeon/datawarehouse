# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the files."""
import datetime
from functools import lru_cache
import pathlib
from urllib.parse import urlparse

from django.conf import settings
from django.db import models
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin as EMOM


class Artifact(EMOM('artifact'), models.Model):
    """Model for Artifact."""

    name = models.CharField(max_length=150)
    url = models.URLField(max_length=400, unique=True)
    valid_for = models.PositiveSmallIntegerField(null=True)
    expiry_date = models.DateTimeField(null=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    @staticmethod
    @lru_cache
    def _get_valid_for(url):
        """
        Get appropiate valid_for value.

        Try to match url with the ones configured in ARTIFACTS_KNOWN_VALID_FOR,
        return ARTIFACTS_DEFAULT_VALID_FOR_DAYS if not found.
        """
        return int(
            settings.ARTIFACTS_KNOWN_VALID_FOR.get(
                url,
                settings.ARTIFACTS_DEFAULT_VALID_FOR_DAYS
            )
        )

    def save(self, *args, **kwargs):
        # pylint: disable=signature-differs
        """
        Override save method.

        Generate name from url if not provided.
        """
        parsed_url = urlparse(self.url)

        if not self.name:
            path = parsed_url.path
            self.name = pathlib.Path(path).name

        if not self.valid_for:
            # Call _get_valid_for with the bucket url (without file path specifics)
            bucket = f"{parsed_url.netloc}/{parsed_url.path[1:].split('/')[0]}"
            self.valid_for = self._get_valid_for(bucket)

        # When created, add an expiry_date if valid_for
        if not self.expiry_date:
            self.expiry_date = (
                timezone.now() + datetime.timedelta(days=self.valid_for)
            )

        super().save(*args, **kwargs)

    @classmethod
    def create_from_url(cls, url):
        """Create Artifact from url."""
        return cls.objects.get_or_create(url=url)[0]
