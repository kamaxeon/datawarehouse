"""
Datawarehouse authorization layer.

Define whether a user has permissions to access certain data.
"""
from operator import attrgetter
import time

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import SAFE_METHODS

from datawarehouse import models

User = get_user_model()


class PolicyAuthorizationBackend:
    """
    Policy based authorization backend.

    Limit user access to objects related data based on the policies it is related to.
    """

    @staticmethod
    def get_path_to_policy(obj):
        """Return the path_to_policy for an object."""
        return getattr(obj, 'path_to_policy', 'policy')

    @staticmethod
    def _is_authorized(request, obj, method):
        """Check if the request is authorized for the object specified."""
        path_to_policy = PolicyAuthorizationBackend.get_path_to_policy(obj.__class__)

        if path_to_policy is None:
            # No relation to policies, public
            return True

        # Replace __ nested objects notation with dots
        path_to_policy = path_to_policy.replace('__', '.')
        policy = attrgetter(path_to_policy)(obj)

        if not policy:
            # No policy, private by default
            return False

        group = getattr(policy, f'{method}_group')
        if not group:
            # Policy has no group, public
            return True

        return group.id in request.session['user_groups']

    @staticmethod
    def is_read_authorized(request, obj):
        """Check if the user authorized to read this object."""
        return PolicyAuthorizationBackend._is_authorized(request, obj, 'read')

    @staticmethod
    def is_write_authorized(request, obj):
        """Check if the user authorized to write this object."""
        return PolicyAuthorizationBackend._is_authorized(request, obj, 'write')

    @staticmethod
    def _filter_authorized(request, queryset, method):
        """
        Filter queryset excluding objects not authorized for method.

        Parameters:
            request: Request object of the API call
            queryset: QuerySet to filter
            method: 'read' or 'write'
        """
        path_to_policy = PolicyAuthorizationBackend.get_path_to_policy(queryset.model)
        if path_to_policy is None:
            return queryset.all()

        auth_filter = (
            Q(**{f'{path_to_policy}__{method}_group': None}) |
            Q(**{f'{path_to_policy}__{method}_group__id__in': request.session['user_groups']})
        )

        return queryset.exclude(**{path_to_policy: None}).filter(auth_filter)

    @staticmethod
    def filter_read_authorized(request, queryset):
        """Filter queryset for with read permissions."""
        return PolicyAuthorizationBackend._filter_authorized(request, queryset, 'read')

    @staticmethod
    def filter_write_authorized(request, queryset):
        """Filter queryset for with write permissions."""
        return PolicyAuthorizationBackend._filter_authorized(request, queryset, 'write')

    @staticmethod
    def filter_authorized(request, queryset):
        """Filter queryset depending on request's method."""
        method = 'read' if request.method in SAFE_METHODS else 'write'
        return PolicyAuthorizationBackend._filter_authorized(request, queryset, method)

    @staticmethod
    def get_users_authorized(obj, method):
        """
        Get list of users authorized to do {method} in the object.

        Parameters:
            obj: Object to get the users from
            method: 'read' or 'write'
        """
        path_to_policy = PolicyAuthorizationBackend.get_path_to_policy(obj.__class__)
        if path_to_policy is None:
            return User.objects.all()

        # Replace __ nested objects notation with dots
        path_to_policy = path_to_policy.replace('__', '.')
        policy = attrgetter(path_to_policy)(obj)

        if not policy:
            # No policy, private by default
            return User.objects.none()

        group = getattr(policy, f'{method}_group')
        if not group:
            # Policy has no group, public
            return User.objects.all()

        return User.objects.filter(groups=group)

    @staticmethod
    def all_objects_authorized(request, queryset):
        """Check if the user is authorized to perform the action on all the objects of the queryset."""
        authorized_objects = PolicyAuthorizationBackend.filter_authorized(request, queryset)
        return queryset.count() == authorized_objects.count()

    @staticmethod
    def get_user_groups(request):
        """Return the list of group ids the user belongs to."""
        return list(request.user.groups.values_list('id', flat=True))

    @staticmethod
    def _get_policies_authorized(request, method):
        """Return a list of policies the user is authorized to do {method}."""
        return models.Policy.objects.filter(
            Q(**{f'{method}_group': None}) |
            Q(**{f'{method}_group__id__in': request.session['user_groups']})
        )

    @staticmethod
    def get_policies_read_authorized(request):
        """Return a list of policies the user is authorized to read."""
        return PolicyAuthorizationBackend._get_policies_authorized(request, 'read')

    @staticmethod
    def get_policies_write_authorized(request):
        """Return a list of policies the user is authorized to write."""
        return PolicyAuthorizationBackend._get_policies_authorized(request, 'write')


class RequestAuthorization:
    """
    RequestAuthorization middleware.

    Inject user authorization data into the session.
    """

    def __init__(self, get_response):
        """Initialize middleware."""
        self.get_response = get_response

    @staticmethod
    def _try_token_auth(request):
        """Try to authenticate user with token."""
        try:
            request.user = TokenAuthentication().authenticate(request)[0]
        except AuthenticationFailed:
            pass

    @staticmethod
    def fill_user_data(request):
        """
        Update session to store user data.

        Django keeps the session after the user logs in/out, so we need to check that
        the data stored corresponds to the request user.

        If the request user is not the same as the stored data, update the data.
        """
        # Workaround for https://gitlab.com/cki-project/datawarehouse/-/issues/108
        if not request.user.id and request.META.get('HTTP_AUTHORIZATION'):
            RequestAuthorization._try_token_auth(request)

        user_id = request.user.id or 'anonymous'
        session_user_id = request.session.get('user_id')

        # Check if the authorization data is still valid
        session_auth_cache_valid = (
            (time.time() - request.session.get('last_updated', 0)) < settings.SESSION_AUTH_CACHE_TTL_S
        )

        if session_user_id == user_id and session_auth_cache_valid:
            # No need to update session data
            return

        request.session['user_id'] = user_id
        request.session['user_groups'] = PolicyAuthorizationBackend.get_user_groups(request)
        request.session['last_updated'] = time.time()

    def __call__(self, request):
        """
        Override __call__ method.

        Before calling get_response, fill the gittree_authorization data.
        """
        self.fill_user_data(request)
        response = self.get_response(request)
        return response
